﻿
namespace SaaSify
{
    /// <summary>
    /// Configuration interface
    /// </summary>
    internal interface ISaaSifyConfiguration
    {
        /// <summary>
        /// If using Gzip Compression
        /// </summary>
        bool Compress { get; set; }

        /// <summary>
        /// If to generate Font files from Svg
        /// </summary>
        bool IncludeSvgGeneration { get; set; }

        /// <summary>
        /// The folder containing svg vector images.
        /// </summary>
        string SvgFolderPath { get; set; }

        /// <summary>
        /// The output of fontfiles.
        /// TODO: This is going to be a challenge to include this package to be indpendent of npm. NodeJS required thogh.
        /// </summary>
        string SvgOutputFolderPath { get; set; }    

        /// <summary>
        /// A list of files to listen to.
        /// </summary>
        IEnumerable<string>? InputFiles { get; set; }

        /// <summary>
        /// The output folder for generated files.
        /// </summary>
        string OutputFolder { get; set; }
    }
}
