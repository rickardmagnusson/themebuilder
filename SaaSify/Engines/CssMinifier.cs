﻿
using System.Text.RegularExpressions;

namespace SaaSify
{
    internal class CssMinifier
    {
        string? _body { get; set; }

        /// <summary>
        /// Construct a new instance of CssMinifier class. 
        /// </summary>
        /// <param name="f"></param>
        /// <exception cref="Exception"></exception>
        public CssMinifier(string f)
        {
            try
            {
                if (File.Exists(f))
                {
                    _body = new StreamReader(f).ReadToEnd();

                    //---------> Execute
                    _body = CleanCss();

                    //write minified css to file. 
                    string outFile = f.Replace(".css", ".min.css");
                    var writer = new StreamWriter(outFile);
                    writer.Write(_body);
                    writer.Close();
                }
            }
            catch
            {
                //We dont want Visual Studio to crash... avoid any errors
                throw new Exception("There's no such file with that name, or the file contains errors.");
            }
        }

        /// <summary>
        /// Clean and removes all whitespaces and comments in css file.
        /// </summary>
        /// <returns></returns>
        private string CleanCss()
        {
            _body = Regex.Replace(_body, @"[a-zA-Z]+#", "#");
            _body = Regex.Replace(_body, @"[\n\r]+\s*", string.Empty);
            _body = Regex.Replace(_body, @"\s+", " ");
            _body = Regex.Replace(_body, @"\s?([:,;{}])\s?", "$1");
            _body = _body.Replace(";}", "}");
            _body = Regex.Replace(_body, @"([\s:]0)(px|pt|%|em)", "$1");

            // Remove comments from CSS
            _body = Regex.Replace(_body, @"/\*[\d\D]*?\*/", string.Empty);

            return _body;
        }
    }
}
