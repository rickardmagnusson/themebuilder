﻿
namespace SaaSify
{
    internal class SaaSifyAttribute : Attribute
    {
        internal string Binary { get; }

        public SaaSifyAttribute(string binary) 
        { 
            Binary = binary;    
        }
    }
}
