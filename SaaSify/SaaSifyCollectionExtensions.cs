﻿
using Microsoft.Extensions.DependencyInjection;

namespace SaaSify
{
    internal static class SaaSifyCollectionExtensions
    {
        /// <summary>
        /// Adds the SaaSify service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="useCompression">If to use compressed (.min.css.gz) files.</param>
        /// <returns>IServiceCollection current</returns>
        public static IServiceCollection AddSaaSify(this IServiceCollection services, bool useCompression)
        {
            if (useCompression)
                services.AddSaaSifyCompressedService();

           return services.AddHostedService<SaaSifyService>();
        }
    }
}
