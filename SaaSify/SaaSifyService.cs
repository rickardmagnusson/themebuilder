﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Diagnostics;

namespace SaaSify
{
    internal sealed class SaaSifyService : IHostedService, IDisposable
    {
        private Process? process;


        public SaaSifyService(IConfiguration configuration, SaaSifyService logger)
        {
            //Read in / out files from ITheme class
            //Start listening on .scss files
            //Run compiler on change event
        }

        /*
         * Maybe need to reconsider to use VS events.
         */
        private void StartListener()
        {

        }


        public Task StartAsync(CancellationToken cancellationToken)
        {
            StartListener();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            if (process != null)
            {
                process.Close();
                process.Dispose();
            }

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            process?.Dispose();
            process = null;
        }
    }
}