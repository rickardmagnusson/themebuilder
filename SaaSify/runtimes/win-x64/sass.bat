@echo off
REM Dart executable and a snapshot of dart-sass.
REM Source https://github.com/sass/dart-sass/releases
REM V. Dart Sass 1.43.4
set SCRIPTPATH=%~dp0
set arguments=%*
"%SCRIPTPATH%\src\dart.exe" "%SCRIPTPATH%\src\sass.snapshot" %arguments%
