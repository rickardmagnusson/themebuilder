﻿

namespace SaaSify
{
    internal interface ICommand
    {
        void Execute();
    }
}
