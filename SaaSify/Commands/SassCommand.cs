﻿
using System.Reflection;

namespace SaaSify
{
    internal class SassCommand : ICommand
    {
        /// <summary>
        /// The assembly (Binary) to use
        /// </summary>
        SaaSifyAttribute? attribute { get; set; } 

        public SassCommand()
        {
            attribute = Assembly.GetEntryAssembly()?.GetCustomAttributes<SaaSifyAttribute>().FirstOrDefault();
        }

        public void Execute()
        {
            
        }

        /// <summary>
        /// Get current binary (Dart)
        /// </summary>
        /// <returns></returns>
        public string? GetCommand()
        {
            return attribute?.Binary;
        }
    }
}
