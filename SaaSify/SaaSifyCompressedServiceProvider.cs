﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using System.IO.Compression;

namespace SaaSify
{
    /// <summary>
    /// Contains services for SaaSify.
    /// </summary>
    internal static class SaaSifyCompressedServiceProvider
    {
        /// <summary>
        /// Adds a Compression service to output files.
        /// </summary>
        /// <param name="services"></param>
        public static void AddSaaSifyCompressedService(this IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options => {
                options.EnableForHttps = true;
                options.Providers.Add<GzipCompressionProvider>();
                options.MimeTypes = new[] {
                            "text/css",
                            "application/javascript" //Will add support for JS later.
                        };
            });
        }

        /// <summary>
        /// Enable compressed files to output response.
        /// </summary>
        /// <param name="app"></param>
        public static void UseSaaSifyCompressedService(this IApplicationBuilder app)
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    IHeaderDictionary headers = context.Context.Response.Headers;
                    string contentType = headers["Content-Type"];

                    /*
                     * Types
                     */
                    if (contentType == "application/x-gzip")
                    {
                        if (context.File.Name.EndsWith("css.gz"))
                        {
                            contentType = "text/css";
                        }

                        headers.Add("Content-Encoding", "gzip");
                        headers["Content-Type"] = contentType;
                    }
                }
            });
        }
    }
}
