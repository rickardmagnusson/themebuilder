﻿# ThemeBuilder

## This project contains functions to automate:
	Fonts from svg's images 
	Minify css files from .css to min.css
	Gzip (Compress) css files into min.css.gz
	Create sass files from svg icons (Construct the sass from codes in a css)

## Requirements
	Dependent on svgtofont (Npm package)

	To install svgtofont globally:

	Run in PS =>
    PS> npm i svgtofont -g 

	The command to create css and font is:
    PS> svgtofont --sources ./svg --output ./font --fontName fa

![ThemeBuilder](ThemeBuilder/assets/themebuilder.png "ThemeBuilder image")


## SASS The C# WAY
	
	The goal is to create a SAAS application.
	By reading a theme configuration for eg. MyTheme.cs.
	Listening to changes on MyTheme.cs or MyTheme.sass, will run the compiler and create the min.css.gz on changes.
	By installing the nuget package into VS and add the service to the application:
	
	//Remeber: Only during debug
	service.AddSaaSify(generator=> {
		generator.Add<MyTheme>();
		generator.Add<MyOtherTheme>()
	});

	The Theme file:

	public interface ITheme
	{
		bool Compress { get; set;}
		bool IncludeSvgGeneration{ get; set; }
		...
		IEnumerable<string> InputFiles{get; set;}
		string OutputFolder{get; set;}
		...
	}