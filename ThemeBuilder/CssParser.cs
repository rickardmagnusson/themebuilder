﻿using System.Text.RegularExpressions;

namespace ThemeBuilder
{
    /// <summary>
    /// Contains extractions of css-classnames and code for an icon.
    /// The name should be IconCssExtractor not CssParser.
    /// </summary>
    internal class CssParser
    {
        /// <summary>
        /// A list of all svg fonts.
        /// </summary>
        public List<FontItem> Items { get; private set; } = new List<FontItem>();

        /// <summary>
        /// Creates a  new instance of CssParser.
        /// </summary>
        /// <param name="cssFile"></param>
        public CssParser(string cssFile)
        {
            Parse(cssFile);
        }

        /// <summary>
        /// Search pattern RegEx pattern
        /// Find a string that starts with .fa- (include all) and end at :before.
        /// </summary>
        private string _pattern = @"\.fa-(.+):before";

        /// <summary>
        /// Match content: include "[.+]"    
        /// .fa-iconName:before {
        ///      content: "code";
        /// }
        /// </summary>
        private string _contentPattern = @"\s*\{\s*content:\s*""\\(.+)"";";

        /// <summary>
        /// Parse a css file that contains fa UI icons. (Fonts)
        /// </summary>
        /// <param name="cssFile">Path to css file</param>
        /// <returns></returns>
        private void Parse(string cssFile)
        {
            var css = File.ReadAllText(cssFile);
            var allPattern = _pattern + _contentPattern;
            var regEx = new Regex(allPattern, RegexOptions.Multiline);

            Items = regEx.Matches(css)
                .Select(match => new FontItem { Class = $"{match.Groups[1].Value.ToLower()}", Code = match.Groups[2].Value.ToLower() })
                .OrderBy(x => x.Class)
                .ToList();
        }
    }
}
