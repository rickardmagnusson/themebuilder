﻿

namespace ThemeBuilder.Commands
{
    [Verb("min", HelpText = "Minifies a css file")]
    public class MinifyCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Executing Minification");

            var path = $"{ThemeExtensions.GetProjectRoot()}{@"\src\css\fonts.css"}";
            Console.WriteLine($"Using file: {path}");

            new CssMinifier(path);

            Console.WriteLine($"Finished minification.");
        }
    }
}