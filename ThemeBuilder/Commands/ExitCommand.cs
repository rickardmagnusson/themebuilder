﻿
namespace ThemeBuilder.Commands
{
    [Verb("exit", HelpText = "Quit and exit program.")]
    internal class ExitCommand : ICommand
    {
        /// <summary>
        /// Executes the exit command.
        /// Will quit and end (exit) the program.
        /// </summary>
        public void Execute()
        {
            Environment.Exit(0);
        }
    }
}
