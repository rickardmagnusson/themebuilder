﻿
namespace ThemeBuilder.Commands
{
    internal interface ICommand
    {
        void Execute();
    }
}
