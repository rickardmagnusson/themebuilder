﻿
namespace ThemeBuilder.Commands
{
    /// <summary>
    /// 
    /// </summary>
    [Verb("sass", HelpText = "Executes and creates a sassfile. Inc. a base scss")]
    public class SassCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Executing Sass");
            var path = $"{ThemeExtensions.GetProjectRoot()}{@"\src\scss\fonts.scss"}"; //TODO.. Replace fonts with name provided from configuration.
            new SassProcessor(path);
            Console.WriteLine("Finished Sass process.");
            Console.WriteLine("Press return to return to main menu.");
            Console.ReadLine();
        }  
    }
}
