﻿

namespace ThemeBuilder.Commands
{
    [Verb("fonts", HelpText = "Executes and creates the font files from svg icons.")]
    public class FontsCommand : ICommand
    {
        /// <summary>
        /// Executes SvgToFonts.
        /// </summary>
        public void Execute()
        {
            Console.WriteLine("Executing conversion of svg to fonts");
            //Not needing the path here, since we know svg icons needs to be in ./src/svg folder.
            new FontItemParser();
        }
    }
}
