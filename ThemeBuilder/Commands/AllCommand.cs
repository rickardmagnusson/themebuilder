﻿

namespace ThemeBuilder.Commands
{
    /// <summary>
    /// Runs the all command.
    /// Will run all command from generate svg to publish "filename.css.min.gz" and the fonts.
    /// </summary>
    [Verb("all", HelpText = "Executes and runs all command to create a Theme.")]
    internal class AllCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Running all commands");

            /**
             * The order is as follows:
             * Create fonts from svg folder. This creates a css file containing icons and fonts in folder fonts.  
             * Create scss file
             * Minify the css created by svgtofont.
             * Compress css file to gz.
             */

            new FontsCommand().Execute();   
            new SassCommand().Execute();
            new MinifyCommand().Execute();
            new CompressCommand().Execute();

            Console.WriteLine("All commands finished.");
        }
    }
}
