﻿
using ThemeBuilder.Configuration;

namespace ThemeBuilder.Commands
{
    [Verb("about", HelpText = "Display more information about this application")]
    internal class AboutCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Some about info");

            new ThemeConfiguration().BuildTags();

            Console.ReadLine();
        }
    }
}
