﻿
namespace ThemeBuilder.Commands
{
    [Verb("zip", HelpText = "Executes and and creates a zipped (css) file.")]
    internal class CompressCommand : ICommand
    {
        /// <summary>
        /// Execute the "zip" command
        /// </summary>
        public void Execute()
        {
            Console.WriteLine("Executing Gzip command.");

            var path = $"{ThemeExtensions.GetProjectRoot()}{@"\src\css\fonts.min.css"}"; //Todo: Replace the fonts
            new GzipCompressor(path);

            Console.WriteLine("Done gzip compressing.");
        }
    }
}
