﻿using System.Diagnostics;

namespace ThemeBuilder
{
    /// <summary>
    /// Contains function to creating fonts.
    /// Attension!! Needs SvgToFont and NodeJS installed. Will solve this later to be included.  
    /// </summary>
    internal class FontItemParser
    {
        string cssFile { get; set; }    

        /// <summary>
        /// Runs a svg to font process.
        /// Create an instance of FontItemParser class
        /// </summary>
        /// <param name="f">The css file to parse</param>
        public FontItemParser()
        {
            Task.Factory.StartNew(async() => 
                await RunCommand(Constants.SVGToFontCommand,
                                 ThemeExtensions.GetProjectRoot()));
        }

        /// <summary>
        /// Opens a new task in cmd and runs the command provided.
        /// </summary>
        /// <param name="commandToRun"></param>
        /// <param name="workingDirectory"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static async Task<string> RunCommand(string commandToRun, string workingDirectory = null)
        {
            //Hack to make the function async. Add pragma... (To be improved. Let us skip the console in VS telling us it's lacks of the await)-> 
            #pragma warning disable 1998
            await Task.Factory.StartNew(async () =>
            {
                if (string.IsNullOrEmpty(workingDirectory))
                {
                    /**
                     * Get the current working dir means to create files 
                     * from this folder, and where to find item.
                     */
                    workingDirectory = Directory.GetDirectoryRoot(Directory.GetCurrentDirectory());
                }

                var processStartInfo = new ProcessStartInfo()
                {
                    FileName = "cmd",
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    WorkingDirectory = workingDirectory
                };

                var process = Process.Start(processStartInfo);

                if (process == null)
                {
                    //Something went totally wrong.
                    throw new Exception("Process should not be null.");
                }

                /**
                 * Run the SvgToFont command as command process
                 */
                process.StandardInput.WriteLine($"{commandToRun} & exit");
                process.WaitForExit();

                return process.StandardOutput.ReadToEnd();
            });
            #pragma warning restore 1998
            return string.Empty;
        }
    }
}
