﻿

namespace ThemeBuilder
{
    internal class Constants
    {
        public const string CssFile = @"fonts\{fileName}.css";
        public const string SassFile = @"scss\{filename}.scss";
        public const string SVGToFontCommand = "svgtofont --sources ./src/svg --output ./dist/fonts/ec --fontName fa"; //TODO: replace fa 
    }
}
