﻿
using System.Text.Json;

namespace ThemeBuilder.Configuration
{
    /// <summary>
    /// SAAS configuration.
    /// By adding this as a service via services.AddTheme<ThemeNameClass>() it will be compiled into a sass file
    /// </summary>
    internal class ThemeConfiguration
    {
        public ThemeConfiguration() { }

        public void BuildTags()
        {
            var theme = Tag.Builder
                            .Add("$primary").Values(new List<string> { "#fff" })
                            .Add("$secondary").Values(new List<string> { "#383838" })
                            .Build();

            var themeConfig = JsonSerializer.Serialize(theme);

            Console.Write(themeConfig);

        }
    }
}
