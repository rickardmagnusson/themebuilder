﻿
/*
 * The function
 * Build a fluent Container that returns a Dictionary of items containing keys and values.
 * Add a key then add tags to that key.
 * Finalize and return the dictionary with a build command.
 */
namespace ThemeBuilder.Configuration
{
    /// <summary>
    /// IKey interface
    /// </summary>
    public interface IKey 
    { 
        /// <summary>
        /// A List<string> Values
        /// </summary>
        /// <param name="values">List of strings</param>
        /// <returns></returns>
        IValues Values(List<string> values);  
    }
    
    /// <summary>
    /// IValues
    /// </summary>
    public interface IValues 
    { 
        /// <summary>
        /// Add a string dictionary(key)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        IKey Add(string key); 

        /// <summary>
        /// Resolve the Tag Builder
        /// </summary>
        /// <returns></returns>
        Dictionary<string, IEnumerable<string>> Build(); 
    }


    /// <summary>
    /// Adds keys, with a List<string> as value.
    /// <example>
    /// Usage:
    ///<code>
    /// Tag.Builder
    ///    .Add("key1").Values(new List<string> { "value1" })
    ///    .Add("key2").Values(new List<string> { "value1" })
    ///    .Build(); // Build() , resolve and return a Dictionary<string, IEnumerable<string>>
    ///</code>
    ///</example>
    /// Tag.Builder Conditional (ContainerBuilder) class
    /// Implemented as Singleton to avoid => Add a IKey to IValues 
    /// </summary>
    public class Tag : IKey, IValues
    {
        static Tag Instance;

        /// <summary>
        /// private instance
        /// </summary>
        private Tag()
        {
            Dictionary = new Dictionary<string, IEnumerable<string>>();
        }

        /// <summary>
        /// Start an instance of Tag
        /// </summary>
        public static IValues Builder
        { 
            get { if (Instance == null) 
                    Instance = new Tag();
                return Instance;
            } 
        }   

        /// <summary>
        /// Dictionary of values
        /// </summary>
        private Dictionary<string, IEnumerable<string>> Dictionary
        {
            get; set;
        }

        /// <summary>
        /// The current dictionary key
        /// </summary>
        private string Key
        {
            get; set;
        }

        /// <summary>
        /// Adds a new key
        /// </summary>
        /// <param name="culture"></param>
        /// <returns>IKey</returns>
        public IKey Add(string key)
        {
            this.Key = key;
            return this;
        }

        /// <summary>
        /// A values to the current IKey
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public IValues Values(List<string> values)
        {
            Dictionary.CreateOrAdd(Key, values);

            return this;
        }

        /// <summary>
        /// Build the container and Resolve the dictionary.
        /// </summary>
        /// <returns>A Dictionary of ketand values</returns>
        public Dictionary<string, IEnumerable<string>> Build()
        {
            return Dictionary;
        }
    }
}
