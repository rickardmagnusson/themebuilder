﻿
namespace ThemeBuilder
{
    /// <summary>
    /// Various extensions.
    /// </summary>
    internal static class ThemeExtensions
    {
        /// <summary>
        /// Extensions for Dictionary.
        /// Add or create a key with values
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="map"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void CreateOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> map, TKey key, TValue value)
        {
            map[key] = value;
        }

        /// <summary>
        /// Makes a IEnumerable<T> async 
        /// </summary>
        /// <typeparam name="T">The type argument.</typeparam>
        /// <param name="list">IEnumerable<T> List</param>
        /// <param name="func">The function to use</param>
        /// <returns>An awaitable item</returns>
        public static async Task ForEachAsync<T>(this List<T> list, Func<T, Task> func)
        {
            foreach (var value in list)
            {
                await func(value);
            }
        }

        /// <summary>
        /// Method to extract helptext from Verb attribute.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string GetCommand<T>()
        {
            System.Reflection.MemberInfo info = typeof(T);
            object[] attributes = info.GetCustomAttributes(true);

            //For loop is still the fastest iterator!!
            for (int i = 0; i < attributes.Length; i++)
            {
                if (attributes[i] is VerbAttribute)
                {
                    var attr = ((VerbAttribute)attributes[i]);
                    return $"{attr.Name} \t: {attr.HelpText}";
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Get project directory path.
        /// Microsoft after all these years haven't included this option as default...
        /// </summary>
        /// <returns></returns>
        public static string GetProjectRoot()
        {
            var url = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");

#if DEBUG
            return url.Replace(@"\bin\Debug\net6.0", ""); //TODO: Get net version
#else
            return url.Replace(@"\bin\Release\net6.0", ""); //TODO: Get net version
#endif
        }
    }
}
