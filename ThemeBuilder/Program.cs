﻿
using ThemeBuilder.Commands;
using Command = ThemeBuilder.ThemeExtensions;

/// <summary>
/// Why write such aprogram?
/// There's a lot of extensions, apps, and NPM's out there.
/// I like control, and use functions that I can control.
/// When their code fails, I still has mine working.
/// 
/// By Bootproject!
/// 
///  ( ^ ^)
///  (|)(||)
///   (+)
///  (|)(||)
///  
/// </summary>
namespace ThemeBuilder { 
    internal class Program
    {
        static Action<string> G = (s) => { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine(s); };
        static Action<string> Y = (s) => { Console.ForegroundColor = ConsoleColor.Yellow; Console.WriteLine(s); };

        /// <summary>
        /// Starts a new instance of ThemeBuilder.
        /// In future make this async.
        /// </summary>
        /// <param name="args"></param>
        /// <exception cref="InvalidOperationException">Will display errors => onerror.</exception>
        internal static void Main(string[] args)
        {
            bool keepGoing = true;
            try
            {
                do
                {
                    var selection = DisplayMenu(args);

                    switch (selection)
                    {
                        case "all":
                            Console.WriteLine("Running (Theme) of css(gzipped) and fonts.");
                            new AllCommand().Execute();
                            break;
                        case "fonts":
                            Console.WriteLine("Running svg to fonts");
                            new SassCommand().Execute();
                            break;
                        case "min":
                            Console.WriteLine("Running Minify files.");
                            new MinifyCommand().Execute();  
                            break;
                        case "zip":
                            Console.WriteLine("Running compress.");
                            new CompressCommand().Execute();
                            break;
                        case "sass":
                            Console.WriteLine("Running creation of sass file(s)");
                            new SassCommand().Execute();    
                            break;
                        case "about":
                            new AboutCommand().Execute();
                            break;
                        case "exit":
                            new ExitCommand().Execute();
                            break;
                        default:
                            throw new InvalidOperationException("You entered an invalid option.");
                    }
                } while (keepGoing);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exeption occurred: {ex.Message}");
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Displays menu choices.
        /// </summary>
        /// <returns>The input from console.</returns>
        static string DisplayMenu(string[] args)
        {
            Console.Clear();
            Console.Title = "ThemeBuilder (2021)";
            G($"Working Directory: {Command.GetProjectRoot()}");
            Y("\n");

            Console.WriteLine(Command.GetCommand<AllCommand>());
            Console.WriteLine(Command.GetCommand<FontsCommand>());
            Console.WriteLine(Command.GetCommand<SassCommand>());
            Console.WriteLine(Command.GetCommand<CompressCommand>());
            Console.WriteLine(Command.GetCommand<MinifyCommand>());
            Console.WriteLine(Command.GetCommand<AboutCommand>());
            Console.WriteLine(Command.GetCommand<ExitCommand>());

            return Console.ReadLine();
        }
    }
}