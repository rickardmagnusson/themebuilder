﻿using System.IO.Compression;

namespace ThemeBuilder
{
    /// <summary>
    /// Compress a css file into a .gz zipped folder.
    /// </summary>
    internal class GzipCompressor
    {
        /// <summary>
        /// Create a new instance of GzipCompressor.
        /// </summary>
        /// <param name="f">The css file to compress</param>
        public GzipCompressor(string f)
        {
            //Execute ---->
            CompressFile(f);
        }

        /// <summary>
        /// Compress file
        /// </summary>
        /// <param name="originalFileName">The css file to compress</param>
        private void CompressFile(string originalFileName)
        {
            if (File.Exists(originalFileName))
            {
                //Taken from MSDN (don't remember the url). // will add it
                string compressedNestedFileName = $"{originalFileName}.gz";
                using FileStream originalFileStream = File.Open(originalFileName, FileMode.Open);
                using FileStream compressedFileStream = File.Create(compressedNestedFileName);
                using var compressor = new GZipStream(compressedFileStream, CompressionMode.Compress);
                originalFileStream.CopyTo(compressor);
            }
            else
            {
                throw new Exception($"The file {originalFileName} doesn't exit!! Check your path and try again.");
            }
        }
    }
}
