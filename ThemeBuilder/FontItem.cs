﻿

namespace ThemeBuilder
{
    /// <summary>
    /// FontItem class
    /// </summary>
    internal class FontItem
    {
        /// <summary>
        /// The css class
        /// </summary>
        public string Class { get; set; }

        /// <summary>
        /// The code found in "content" of a css class.
        /// </summary>
        public string Code { get; set; }
    }
}
